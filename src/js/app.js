import "../scss/app.scss";

window.addEventListener("DOMContentLoaded", () => {
  // This block will be executed once the page is loaded and ready

  const elements = document.querySelectorAll('.card');
  const elementsArr=Array.from(elements);
  console.log(elementsArr)

  setTimeout(function(){
    elementsArr.forEach(element=>{
      console.log(element)
      if (!element.classList.contains('active')) {
        element.style.display='none';
      }
    })
  }, 3000);

});
